﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MurmurHash;
using System.Windows.Media.Imaging;
using System.IO;

namespace MurmurExperiments
{
    class Program
    {
        static void Main(string[] args)
        {
            var m = new Murmur3();
            var b = new byte[] {
                0,1,2
            };
            var r1 = m.ComputeHash(b);
            var r2 = m.ComputeHash(b);

            Console.WriteLine("r1: {0}", BitConverter.ToString(r1));
            Console.WriteLine("r2: {0}", BitConverter.ToString(r2));

            using (var f = new FileStream(@"C:\Users\tferrell\Desktop\IMAGE_00002.jpg", FileMode.Open, FileAccess.Read))
            {
                var j = new JpegBitmapDecoder(f, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                Console.WriteLine(j.Metadata.DateTaken);
            }


            return;
        }
    }
}
